package darkyenus.utils.net;

import java.net.Socket;

/**
 * @author Darkyen
 */
public abstract class HTTPServer {
    private int port;

    public HTTPServer(int port) {
        this.port = port;
    }

    public abstract void onRequest();

    //---------------------- classes

    public static class Connection extends Thread {
        private Socket connected;

        public Connection(Socket with) {
            connected = with;
        }

        @Override
        public void run() {

        }
    }
}
