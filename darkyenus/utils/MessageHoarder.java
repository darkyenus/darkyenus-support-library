package darkyenus.utils;

import darkyenus.utils.collections.Pair;

import java.util.ArrayList;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/29/13
 * Time: 6:31 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class MessageHoarder {
    private ArrayList<Pair<Integer, String>> messages = new ArrayList<Pair<Integer, String>>();

    public void message(String message) {
        if (messages.isEmpty()) {
            messages.add(new Pair<Integer, String>(1, message));
        } else {
            if (messages.get(messages.size() - 1).getSecond().equals(message)) {
                messages.get(messages.size() - 1).setFirst(messages.get(messages.size() - 1).getFirst() + 1);
            } else {
                messages.add(new Pair<Integer, String>(1, message));
            }
        }
    }

    public String getMessages(String separator) {
        if (messages.isEmpty()) {
            return "";
        } else {
            StringBuilder result = new StringBuilder();
            result.append(messages.get(0).getSecond());
            for (int i = 1; i < messages.size(); i++) {
                result.append(separator);
                if (messages.get(i).getFirst() != 1) {
                    result.append(messages.get(i).getFirst()).append(" times: ");
                }
                result.append(messages.get(i).getSecond());
            }
            return result.toString();
        }
    }

    public void resetMessages() {
        messages.clear();
    }

    public void removeOldMessages(int howMuchOldMessages) {
        int toRemove = howMuchOldMessages;
        while (toRemove > 0 && !messages.isEmpty()) {
            messages.remove(0);
            toRemove--;
        }
    }

    public int getUniqueMessageCount() {
        return messages.size();
    }
}
