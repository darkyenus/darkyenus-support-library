package darkyenus.utils;

import java.util.Random;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/16/13
 * Time: 12:07 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class NoiseUtil {
    public static float cosInterpolate(float a, float b, float x) {
        float ft = x * (float) Math.PI;
        float f = (float) (1 - Math.cos(ft)) * 0.5f;
        return a * (1 - f) + b * f;
    }

    /**
     * Returns random perlin distributed number from -1 to 1.
     * Peaks are at integers, everything between is cos smoothed.
     *
     * @param seed seed to use
     * @param a    value to get
     * @return random number
     */
    public static float getPerlin(long seed, float a) {
        Random first = new Random(seed + (long) Math.floor(a));
        Random second = new Random(seed + (long) Math.floor(a) + 1);
        return cosInterpolate(first.nextFloat(), second.nextFloat(), a % 1f);
    }
}
