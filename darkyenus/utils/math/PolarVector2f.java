package darkyenus.utils.math;

/**
 * Immutable vector saved in polar coordinates of float precision.
 *
 * @author Darkyen
 */
public class PolarVector2f {

    /**
     * Angle in radians (final)
     */
    private float angle;
    /**
     * Size of vector (final)
     */
    private float magnitude;

    /**
     * Construct a new PolarVector2d
     *
     * @param angle     angle of vector in radians
     * @param magnitude magnitude of vector
     */
    public PolarVector2f(float angle, float magnitude) {
        this.angle = angle;
        this.magnitude = magnitude;
    }

    /**
     * For serializing only.
     */
    private PolarVector2f() {
    }

    public static PolarVector2f createFromCoordinates(float x, float y) {
        double rawAngle = Math.atan2(x, -y);
        double angle = Double.isNaN(rawAngle) ? 0 : rawAngle;
        double magnitude = Math.pow(x * x + y * y, 0.5);
        return new PolarVector2f((float) angle, (float) magnitude);
    }

    /**
     * Get angle of vector
     *
     * @return angle in radians
     */
    public float getAngle() {
        return angle;
    }

    /**
     * Get magnitude of vector
     *
     * @return size of vector
     */
    public float getMagnitude() {
        return magnitude;
    }

    /**
     * Copy this vector with new angle.
     *
     * @param angle new angle in radians
     * @return result
     */
    public PolarVector2f copySetAngle(float angle) {
        return new PolarVector2f(angle, magnitude);
    }

    /**
     * Copy this vector with new magnitude.
     *
     * @param magnitude new size
     * @return result
     */
    public PolarVector2f copySetMagnitude(float magnitude) {
        return new PolarVector2f(angle, magnitude);
    }

    /**
     * Get x axis size of vector.
     *
     * @return x size
     */
    public float getX() {
        return (float) Math.sin(angle) * magnitude;
    }

    /**
     * Get y axis size of vector.
     *
     * @return y size
     */
    public float getY() {
        return (float) -Math.cos(angle) * magnitude;
    }

    /**
     * Creates new vector which is same as this vector added to given one.
     *
     * @param vector vector to add
     * @return result
     */
    public PolarVector2f addVector(PolarVector2f vector) {
        return PolarVector2f.createFromCoordinates(getX() + vector.getX(), getY() + vector.getY());
    }

    /**
     * Creates new vector which is same as this vector added to given one.
     *
     * @param vector vector to add
     * @param ratio  Vectors will be mixed in this ration
     * @return result
     */
    public PolarVector2f addVector(PolarVector2f vector, float ratio) {
        return PolarVector2f.createFromCoordinates((getX() * ratio) + (vector.getX() * (1 - ratio)), (getY() * ratio) + (vector.getY() * (1 - ratio)));
    }

    /**
     * Creates new vector which is equal to this, but multiplied.
     *
     * @param multiplier multiplier
     * @return result
     */
    public PolarVector2f multiply(float multiplier) {
        return new PolarVector2f(angle, getMagnitude() * multiplier);
    }

    /**
     * Create exact copy of vector
     *
     * @return copy of vector
     */
    public PolarVector2f copy() {
        return new PolarVector2f(angle, magnitude);
    }
}
