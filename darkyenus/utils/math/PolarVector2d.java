package darkyenus.utils.math;

/**
 * Immutable vector saved in polar coordinates of double precision.
 *
 * @author Darkyen
 */
public class PolarVector2d implements Vector2d {

    /**
     * Angle in radians (final)
     */
    private double angle;
    /**
     * Size of vector (final)
     */
    private double magnitude;

    /**
     * Construct a new PolarVector2d
     *
     * @param angle     angle of vector in radians
     * @param magnitude magnitude of vector
     */
    public PolarVector2d(double angle, double magnitude) {
        this.angle = angle;
        this.magnitude = magnitude;
    }

    /**
     * For serializing only.
     */
    private PolarVector2d() {
    }

    public static PolarVector2d createFromCoordinates(double x, double y) {
        double rawAngle = Math.atan2(x, -y);
        double angle = Double.isNaN(rawAngle) ? 0 : rawAngle;
        double magnitude = Math.pow(x * x + y * y, 0.5);
        return new PolarVector2d(angle, magnitude);
    }

    /**
     * Get angle of vector
     *
     * @return angle in radians
     */
    @Override
    public double getAngle() {
        return angle;
    }

    /**
     * Get magnitude of vector
     *
     * @return size of vector
     */
    @Override
    public double getMagnitude() {
        return magnitude;
    }

    /**
     * Copy this vector with new angle.
     *
     * @param angle new angle in radians
     * @return result
     */
    public PolarVector2d copySetAngle(double angle) {
        return new PolarVector2d(angle, magnitude);
    }

    /**
     * Copy this vector with new magnitude.
     *
     * @param magnitude new size
     * @return result
     */
    public PolarVector2d copySetMagnitude(double magnitude) {
        return new PolarVector2d(angle, magnitude);
    }

    /**
     * Get x axis size of vector.
     *
     * @return x size
     */
    @Override
    public double getX() {
        return Math.sin(angle) * magnitude;
    }

    /**
     * Get y axis size of vector.
     *
     * @return y size
     */
    @Override
    public double getY() {
        return -Math.cos(angle) * magnitude;
    }

    /**
     * Creates new vector which is same as this vector added to given one.
     *
     * @param vector vector to add
     * @return result
     */
    public PolarVector2d addVector(PolarVector2d vector) {
        return PolarVector2d.createFromCoordinates(getX() + vector.getX(), getY() + vector.getY());
    }

    /**
     * Creates new vector which is same as this vector added to given one.
     *
     * @param vector vector to add
     * @param ratio  Vectors will be mixed in this ration
     * @return result
     */
    public PolarVector2d addVector(PolarVector2d vector, float ratio) {
        return PolarVector2d.createFromCoordinates((getX() * ratio) + (vector.getX() * (1 - ratio)), (getY() * ratio) + (vector.getY() * (1 - ratio)));
    }

    /**
     * Creates new vector which is equal to this, but multiplied.
     *
     * @param multiplier multiplier
     * @return result
     */
    public PolarVector2d multiply(double multiplier) {
        return new PolarVector2d(angle, getMagnitude() * multiplier);
    }

    /**
     * Create exact copy of vector
     *
     * @return copy of vector
     */
    public PolarVector2d copy() {
        return new PolarVector2d(angle, magnitude);
    }
}
