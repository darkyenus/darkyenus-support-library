package darkyenus.utils.math;

/**
 * @author Darkyen
 */
public interface Vector2d {
    public double getAngle();

    public double getMagnitude();

    public double getX();

    public double getY();
}
