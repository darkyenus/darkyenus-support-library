package darkyenus.utils.math;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/19/13
 * Time: 6:49 PM
 */
public class AutotuningAverager implements Averager{
    private double average = 0;
    private int maxAverages;
    private int averaged = 0;

    public AutotuningAverager(int maxAverages) {
        this.maxAverages = maxAverages;
    }

    public double addValue(double value) {
        average = ((average * averaged) + value) / (averaged + 1);
        if(averaged < maxAverages){
            averaged++;
        }
        return average;
    }

    public double getAverage() {
        return average;
    }
}
