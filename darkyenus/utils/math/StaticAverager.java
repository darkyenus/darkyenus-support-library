package darkyenus.utils.math;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/5/13
 * Time: 8:43 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class StaticAverager implements Averager {
    private double average = 0;
    private long averaged = 0;

    public double addValue(double value) {
        average = ((average * averaged) + value) / (averaged + 1);
        averaged++;
        return average;
    }

    public double getAverage() {
        return average;
    }

    public double getTotal() {
        return average * averaged;
    }

    public long getCount() {
        return averaged;
    }
}
