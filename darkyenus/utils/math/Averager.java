package darkyenus.utils.math;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/19/13
 * Time: 7:21 PM
 */
public interface Averager {
    public double addValue(double value);

    public double getAverage();
}
