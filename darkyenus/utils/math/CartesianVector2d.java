package darkyenus.utils.math;

/**
 * Two dimensional vector, using doubles.
 * Data saved in x and y size.
 *
 * @author Darkyen
 */
public final class CartesianVector2d implements Vector2d {
    private double x;
    private double y;

    private double prefferedAngle = 0;
    private double preferredMagnitude = 0;

    /**
     * Creates new vector with coordinate sizes
     *
     * @param x x axis size
     * @param y y axis size
     */
    public CartesianVector2d(double x, double y) {
        this.x = x;
        this.y = y;
    }

    private void recalculateAngle() {
        double angle = Math.atan2(x, -y);
        prefferedAngle = Double.isNaN(angle) ? 0 : angle;
    }

    /**
     * Get angle of vector
     *
     * @return angle in radians
     */
    @Override
    public double getAngle() {
        return prefferedAngle;
    }

    private void recalculateMagnitude() {
        preferredMagnitude = Math.pow(Math.pow(x, 2) + Math.pow(y, 2), 0.5);
    }

    /**
     * Get magnitude of vector
     *
     * @return size of vector
     */
    @Override
    public double getMagnitude() {
        return preferredMagnitude;
    }

    /**
     * Set angle of vector.
     * By recalculating x and y
     *
     * @param angle new angle in radians
     * @return this, for chaining
     */
    public CartesianVector2d setAngle(double angle) {
        double magnitude = getMagnitude();
        x = Math.sin(angle) * magnitude;
        y = -Math.cos(angle) * magnitude;
        prefferedAngle = angle;
        return this;
    }

    /**
     * Set magnitude of vector.
     * By recalculating x and y
     *
     * @param magnitude new size
     * @return this, for chaining
     */
    public CartesianVector2d setMagnitude(double magnitude) {
        double angle = getAngle();
        //if(angle != 0)System.out.println(angle);
        x = Math.sin(angle) * magnitude;
        y = -Math.cos(angle) * magnitude;
        preferredMagnitude = magnitude;
        return this;
    }

    /**
     * Get x axis size of vector.
     *
     * @return x size
     */
    @Override
    public double getX() {
        return x;
    }

    /**
     * Get y axis size of vector.
     *
     * @return y size
     */
    @Override
    public double getY() {
        return y;
    }

    /**
     * Set x axis size of vector
     *
     * @param x new x
     * @return this, for chaining
     */
    public CartesianVector2d setX(double x) {
        this.x = x;
        recalculateAngle();
        recalculateMagnitude();
        return this;
    }

    /**
     * Set y axis size of vector
     *
     * @param y new y
     * @return this, for chaining
     */
    public CartesianVector2d setY(double y) {
        this.y = y;
        recalculateAngle();
        recalculateMagnitude();
        return this;
    }

    /**
     * Adds given vector to this vector.
     * Only this vector is changed.
     *
     * @param vector vector to add
     * @return this, for chaining
     */
    public CartesianVector2d addVector(CartesianVector2d vector) {
        x += vector.getX();
        y += vector.getY();
        recalculateAngle();
        recalculateMagnitude();
        return this;
    }

    /**
     * Multiplies x and y size of this vector.
     *
     * @param multiplier multiplier
     * @return this, for chaining
     */
    public CartesianVector2d multiply(double multiplier) {
        x *= multiplier;
        y *= multiplier;
        recalculateMagnitude();
        return this;
    }

    /**
     * Create exact copy of vector
     *
     * @return copy of vector
     */
    public CartesianVector2d copy() {
        return new CartesianVector2d(x, y);
    }
}