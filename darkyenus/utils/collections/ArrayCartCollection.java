package darkyenus.utils.collections;

import java.util.*;

/**
 * Array backed CartCollection implementation.
 * Offers even faster iterating than LinkedCartCollection, due of its use of array.
 * Add and remove times should be comparable to LinkedCartCollection implementation,
 * however those operations are not so flexible.
 * <p/>
 * Private property.
 * User: Darkyen
 * Date: 8/27/13
 * Time: 3:39 PM
 */
@SuppressWarnings({"unchecked", "UnusedDeclaration"})
public class ArrayCartCollection<M> implements CartCollection<M> {

    private ArrayCart<M>[] backingArray;
    private int elementCount = 0;

    public ArrayCartCollection() {
        this(64);//Expecting to host big amounts, space is cheap
    }

    public ArrayCartCollection(int initialSize) {
        assert initialSize >= 1;
        backingArray = (ArrayCart<M>[]) new ArrayCart[initialSize];
    }

    @Override
    public Cart<M> add(M entry) {
        ensureAccessible(elementCount);
        backingArray[elementCount] = new ArrayCart<M>(elementCount, entry);
        return backingArray[elementCount++];
    }

    @Override
    public Cart<M> getFirst() {
        if (elementCount > 0) {
            return backingArray[0];
        } else {
            return null;
        }
    }

    @Override
    public Cart<M> getLast() {
        if (elementCount > 0) {
            return backingArray[elementCount - 1];
        } else {
            return null;
        }
    }

    @Override
    public int getSize() {
        return elementCount;
    }

    @Override
    public void clear() {
        elementCount = 0;
    }

    private void ensureAccessible(int index) {
        if (index >= backingArray.length) {
            ArrayCart<M>[] newBackingArray = (ArrayCart<M>[]) new ArrayCart[backingArray.length * 2];
            System.arraycopy(backingArray, 0, newBackingArray, 0, elementCount);
            backingArray = newBackingArray;
        }
    }

    /**
     * Gives fail-fast iterator that does not support removing elements.
     * Do not remove or add elements while iterating.
     *
     * @return iterator over this collection
     */
    @Override
    public Iterator<Cart<M>> iterator() {
        return new Iterator<Cart<M>>() {
            private int activeIndex = -1;
            private int elementCountWhenStarting = elementCount;

            @Override
            public boolean hasNext() {
                return activeIndex + 1 != elementCount;
            }

            @Override
            public Cart<M> next() {
                if (elementCount != elementCountWhenStarting) {
                    throw new ConcurrentModificationException();
                }
                if (!hasNext()) {
                    throw new NoSuchElementException();
                } else {
                    activeIndex++;
                    return backingArray[activeIndex];
                }
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    private class ArrayCart<M> implements Cart<M> {

        private M entry;
        private int index;
        private List<Removable> attachedCarts;

        private ArrayCart(int index, M entry) {
            this.index = index;
            this.entry = entry;
        }

        @Override
        public M getEntry() {
            return entry;
        }

        @Override
        public void attach(Removable cart) {
            if (attachedCarts == null) {
                attachedCarts = new LinkedList<Removable>();
            }
            attachedCarts.add(cart);
        }

        @Override
        public M remove() {
            if (index == -1) {
                throw new IllegalStateException();
            } else {
                elementCount--;
                if (elementCount != index && elementCount != 0) {
                    backingArray[index] = backingArray[elementCount];
                    backingArray[index].index = index;
                    index = -1;
                }//Else: It was last or only, no moving needed

                if (attachedCarts != null) {
                    for (Removable cartToRemove : attachedCarts) {
                        cartToRemove.remove();
                    }
                }
                return entry;
            }
        }
    }
}
