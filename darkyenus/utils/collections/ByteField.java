package darkyenus.utils.collections;

import java.io.*;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class ByteField {

    private byte[] field;

    public ByteField(int capacity) {
        field = new byte[capacity];
    }

    public ByteField() {
        this(1024);
    }

    public void set(int position, byte data) {
        field[position] = data;
    }

    public byte get(int position) {
        return field[position];
    }

    public void load(File file) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        fileInputStream.read(field);
        try {
            fileInputStream.close();
        } catch (Exception e) {
        }
    }

    public void save(File file) throws IOException {
        if (file.getParentFile() != null) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(field);
        fileOutputStream.flush();
        try {
            fileOutputStream.close();
        } catch (Exception e) {
        }
    }
}
