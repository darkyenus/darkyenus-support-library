package darkyenus.utils.collections;

/**
 * Immutable two object pair.
 * <p/>
 * Private property.
 * User: Darkyen
 * Date: 3/25/13
 * Time: 9:07 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class Pair<A, B> {

    private A first;
    private B second;

    public Pair(A first, B second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Serialization constructor.
     */
    private Pair() {
    }

    public A getFirst() {
        return first;
    }

    public B getSecond() {
        return second;
    }

    public void setFirst(A newFirst) {
        this.first = newFirst;
    }

    public void setSecond(B newSecond) {
        this.second = newSecond;
    }
}
