package darkyenus.utils.collections;

/**
 * Private property.
 * User: Darkyen
 * Date: 6/8/13
 * Time: 3:44 PM
 */
public interface Container<M> {
    public M getEntry();
}
