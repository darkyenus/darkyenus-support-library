package darkyenus.utils.collections;

import java.util.Collection;
import java.util.Iterator;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/31/13
 * Time: 8:23 AM
 */
@SuppressWarnings({"UnusedDeclaration", "NullableProblems"})
public class Bag<T> implements Collection<T> {

    private Object[] list;
    private int size = 0;

    public Bag() {
        this(16);
    }

    public Bag(int size) {
        list = new Object[size];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (list[i].equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int position = -1;

            @Override
            public boolean hasNext() {
                return position + 1 < size;
            }

            @Override
            public T next() {
                position++;
                //noinspection unchecked
                return (T) list[position];
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Removal not supported.");
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[size];
        System.arraycopy(list, 0, result, 0, size);
        return result;
    }

    @Override
    public <T1> T1[] toArray(T1[] t1s) {
        Object[] result = t1s.length == size ? t1s : new Object[size];
        System.arraycopy(list, 0, result, 0, size);
        //noinspection unchecked
        return (T1[]) result;
    }

    @Override
    public boolean add(T t) {
        if (list.length == size) {
            Object[] newList = new Object[list.length * 2];
            System.arraycopy(list, 0, newList, 0, list.length);
            list = newList;
        }
        list[size] = t;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (list[i].equals(o)) {
                if (size - 1 > i) {
                    list[i] = list[size - 1];
                    size--;
                } else {
                    list[i] = null;
                    size--;
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        for (Object object : objects) {
            if (!contains(object)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> ts) {
        for (T object : ts) {
            add(object);
        }
        return !ts.isEmpty();
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        for (Object object : objects) {
            remove(object);
        }
        return !objects.isEmpty();
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        throw new UnsupportedOperationException("retainAll not supported");
    }

    @Override
    public void clear() {
        size = 0;
    }
}
