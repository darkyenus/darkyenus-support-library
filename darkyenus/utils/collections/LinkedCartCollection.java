package darkyenus.utils.collections;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class LinkedCartCollection<M> implements CartCollection<M> {

    private LinkedCart<M> head = new LinkedCart<M>(this, null, null, null);
    private LinkedCart<M> tail = new LinkedCart<M>(this, null, head, null);
    private int elementCount = 0;

    {
        head.child = tail;
        head.isFiller = true;
        tail.isFiller = true;
    }

    @Override
    public LinkedCart<M> add(M entry) {
        LinkedCart<M> cart = new LinkedCart<M>(this, entry, tail.parent, tail);
        tail.parent.child = cart;
        tail.parent = cart;
        elementCount++;
        return cart;
    }

    @Override
    public LinkedCart<M> getFirst() {
        if (head.child.isFiller) {
            return null;
        } else {
            return head.child;
        }
    }

    @Override
    public LinkedCart<M> getLast() {
        if (tail.parent.isFiller) {
            return null;
        } else {
            return tail.parent;
        }
    }

    @Override
    public int getSize() {
        return elementCount;
    }

    @Override
    public void clear() {
        elementCount = 0;
        head.child = tail;
        tail.parent = head;
        head.isFiller = true;
        tail.isFiller = true;
    }

    @Override
    public Iterator<Cart<M>> iterator() {
        return new Iterator<Cart<M>>() {

            private LinkedCart<M> active = head;

            @Override
            public boolean hasNext() {
                return !active.child.isFiller;
            }

            @Override
            public Cart<M> next() {
                if (active.child.isFiller) {
                    throw new NoSuchElementException();
                }
                active = active.child;
                return active;
            }

            @Override
            public void remove() {
                active.remove();
            }
        };
    }

    @SuppressWarnings("UnusedDeclaration")
    public static class LinkedCart<M> implements CartCollection.Cart<M> {

        LinkedCartCollection<M> owner;
        private M entry;
        private LinkedCart<M> parent;
        private LinkedCart<M> child;
        private boolean isFiller;
        private List<Removable> attachedCarts;

        /**
         * Only for serialization.
         */
        private LinkedCart() {
        }

        private LinkedCart(LinkedCartCollection<M> owner, M entry, LinkedCart<M> parent, LinkedCart<M> child) {
            this.owner = owner;
            this.entry = entry;
            this.parent = parent;
            this.child = child;
        }

        @Override
        public M getEntry() {
            return entry;
        }

        public void setEntry(M entry) {
            this.entry = entry;
        }

        @Override
        public void attach(Removable cart) {
            if (attachedCarts == null) {
                attachedCarts = new LinkedList<Removable>();
            }
            attachedCarts.add(cart);
        }

        @Override
        public M remove() {
            if (!isFiller) {
                parent.child = child;
                child.parent = parent;


                if (attachedCarts != null) {
                    for (Removable cartToRemove : attachedCarts) {
                        cartToRemove.remove();
                    }
                }
                owner.elementCount--;
                isFiller = true;
                return entry;
            } else {
                throw new IllegalStateException();
            }
        }

        /**
         * Adds new entry into list right after this one.
         *
         * @param entry data
         */
        public LinkedCart<M> add(M entry) {
            LinkedCart<M> cart = new LinkedCart<M>(owner, entry, this, child);
            child.parent = cart;
            child = cart;
            owner.elementCount++;
            return cart;
        }

        public LinkedCart<M> getNext() {
            if (child.isFiller) {
                return null;
            } else {
                return child;
            }
        }

        public LinkedCart<M> getPrevious() {
            if (parent.isFiller) {
                return null;
            } else {
                return parent;
            }
        }
    }
}
