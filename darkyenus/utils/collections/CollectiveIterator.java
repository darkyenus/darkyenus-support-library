package darkyenus.utils.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Darkyen
 */
@SuppressWarnings({"unchecked", "UnusedDeclaration"})
public class CollectiveIterator<T> implements Iterator<T> {

    private Iterator<T>[] iterators;
    private int atIterator = 0;

    public CollectiveIterator(Iterable<T>... iterables) {
        iterators = new Iterator[iterables.length];
        for (int i = 0; i < iterables.length; i++) {
            iterators[i] = iterables[i].iterator();
        }
    }

    public CollectiveIterator(Iterator<T>... iterators) {
        this.iterators = iterators;
    }

    public CollectiveIterator() {
        iterators = new Iterator[0];
    }

    @Override
    public boolean hasNext() {
        return hasIteratorNext(atIterator);
    }

    private boolean hasIteratorNext(int iteratorIndex) {
        return iteratorIndex < iterators.length && (iterators[iteratorIndex].hasNext() || hasIteratorNext(iteratorIndex + 1));
    }

    @Override
    public T next() {
        while (atIterator < iterators.length) {
            if (iterators[atIterator].hasNext()) {
                return iterators[atIterator].next();
            } else {
                atIterator++;
            }
        }
        throw new NoSuchElementException("There aren't any more elements.");
    }

    @Override
    public void remove() {
        iterators[atIterator].remove();
    }

}
