package darkyenus.utils.collections;

import java.util.ArrayList;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class SortedLinkedCartCollection<M> {

    private SortedCart<M> head = new SortedCart<M>(this, null, null, null, Integer.MIN_VALUE);
    private SortedCart<M> tail = new SortedCart<M>(this, null, head, null, Integer.MAX_VALUE);
    private int elementCount = 0;

    {
        head.child = tail;
        head.isFiller = true;
        tail.isFiller = true;
    }

    public SortedCart<M> add(M entry, int sortingTag) {
        SortedCart<M> workingCart = head;
        while (workingCart.child.getSorterTag() < sortingTag) {
            workingCart = workingCart.child;
        }
        return workingCart.add(entry, sortingTag);
    }

    public SortedCart<M> ensure(M entry, int sortingTag) {
        SortedCart<M> workingCart = head;
        if (workingCart.equals(entry)) {
            return workingCart;
        }
        while (workingCart.child.getSorterTag() < sortingTag) {
            workingCart = workingCart.child;
            if (workingCart.equals(entry)) {
                return workingCart;
            }
        }
        return workingCart.add(entry, sortingTag);
    }

    public SortedCart<M> getFirst() {
        if (head.child.isFiller) {
            return null;
        } else {
            return head.child;
        }
    }

    public SortedCart<M> getLast() {
        if (tail.parent.isFiller) {
            return null;
        } else {
            return tail.parent;
        }
    }

    public int getSize() {
        return elementCount;
    }

    public static class SortedCart<M> implements CartCollection.Cart<M> {

        private SortedLinkedCartCollection<M> owner;
        private M entry;
        private SortedCart<M> parent;
        private SortedCart<M> child;
        private boolean isFiller;
        private ArrayList<Removable> attachedCarts;
        private int sorterTag;

        /**
         * Only for serialization.
         */
        private SortedCart() {
        }

        private SortedCart(SortedLinkedCartCollection<M> owner, M entry, SortedCart<M> parent, SortedCart<M> child, int sorterTag) {
            this.owner = owner;
            this.entry = entry;
            this.parent = parent;
            this.child = child;
            this.sorterTag = sorterTag;
        }

        @Override
        public M getEntry() {
            return entry;
        }

        @Override
        public void attach(Removable cart) {
            if (attachedCarts == null) {
                attachedCarts = new ArrayList<Removable>();
            }
            attachedCarts.add(cart);
        }

        /**
         * Removes this cart from list and all its attached carts too.
         *
         * @return entry value held by this cart
         */
        @Override
        public M remove() {
            parent.child = child;
            child.parent = parent;
            if (attachedCarts != null) {
                for (Removable cartToRemove : attachedCarts) {
                    cartToRemove.remove();
                }
            }
            owner.elementCount--;
            return entry;
        }

        /**
         * Adds new entry into list right after this one. NOTE: Sorter tag is
         * actually ignored here.
         *
         * @param entry     cart data
         * @param sorterTag ignored
         */
        private SortedCart<M> add(M entry, int sorterTag) {
            SortedCart<M> cart = new SortedCart<M>(owner, entry, this, child, sorterTag);
            child.parent = cart;
            child = cart;
            owner.elementCount++;
            return cart;
        }

        public SortedCart<M> getNext() {
            if (child.isFiller) {
                return null;
            } else {
                return child;
            }
        }

        public SortedCart<M> getPrevious() {
            if (parent.isFiller) {
                return null;
            } else {
                return parent;
            }
        }

        public int getSorterTag() {
            return sorterTag;
        }
    }
}
