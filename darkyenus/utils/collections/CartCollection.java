package darkyenus.utils.collections;

/**
 * Private property.
 * User: Darkyen
 * Date: 8/28/13
 * Time: 11:08 AM
 */
@SuppressWarnings("UnusedDeclaration")
public interface CartCollection<M> extends Iterable<CartCollection.Cart<M>> {

    public Cart<M> add(M entry);

    public Cart<M> getFirst();

    public Cart<M> getLast();

    public int getSize();

    public void clear();

    public interface Cart<M> extends Removable<M>, Container<M> {

        public void attach(Removable cart);

    }
}
