package darkyenus.utils.collections;

import darkyenus.utils.files.Reader;
import darkyenus.utils.files.Writer;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

/**
 * Class used as savable HashMap for strings.
 * Saved format is human readable.
 * <p/>
 * Format specification:
 * <p/>
 * key1 distributor data1
 * keyN distributor dataN
 * <p/>
 * Where key is HashMap key and data is HashMap data.
 * Default distributor is ':' (without ''s) and there is not a space between key, distributor and data.
 * Format supports comments, both # and // is valid comment line starter.
 * <p/>
 * Example:
 * AmountOfHats:OverNineThousand
 * //Previous line is not very helpful
 * AnotherAmount:9001
 * #That line has string containing number as data!
 * <p/>
 * All characters are allowed,
 * except for character/string used as distributor (but only in key, data can contain distributor),
 * comments tags (# and //) but only on start of line, anywhere else is fine
 * and newline characters, as they are used to split entry pairs.
 * Blank lines are allowed and does not contribute to resulting data structure.
 * <p/>
 * Comments nor blank lines will be saved, use them only when file is read only for that reason or you will lose them.
 *
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class StringMap extends HashMap<String, String> {
    private String distributor = ":";

    /**
     * If file does not exists then will be created with initialValues inside.
     * Then load method will be called.
     *
     * @param path          to file to load or create
     * @param initialValues to put in file that has been just created
     * @throws IOException
     */
    public void loadOrCreate(File path, String[] initialValues) throws IOException {
        if (!path.isFile()) new Writer(path).writeLines(initialValues);
        load(path);
    }

    public void loadOrCreate(String path, String[] initialValues) throws IOException {
        loadOrCreate(new File(path), initialValues);
    }

    /**
     * Loads file into hashMap.
     * Format should be same as saved, see save method.
     *
     * @param path to file to load
     * @throws IOException
     */
    public void load(File path) throws IOException {
        Reader reader = new Reader(path);
        String loaded = reader.readNext();
        while (loaded != null) {
            if (!loaded.startsWith(distributor) && loaded.contains(distributor)
                    && !loaded.startsWith("#") && !loaded.startsWith("//") && !loaded.trim().isEmpty()) {
                int indexOfDistributor = loaded.indexOf(distributor);
                if (!loaded.endsWith(distributor))
                    put(loaded.substring(0, indexOfDistributor), loaded.substring(indexOfDistributor + 1));
                else put(loaded.substring(0, indexOfDistributor), "");
            }
            loaded = reader.readNext();
        }
    }

    public void load(String path) throws IOException {
        load(new File(path));
    }

    /**
     * This will save entire hashMap into file using pattern node-distributor-data (without commas)
     *
     * @param path path to file
     * @throws IOException - if error in writing file occurs
     */
    public void save(File path) throws IOException {
        Set<String> strings = keySet();
        String[] keys = strings.toArray(new String[strings.size()]);
        String[] saveSet = new String[keys.length];
        for (int i = 0; i < saveSet.length; i++) {
            saveSet[i] = keys[i] + distributor + get(keys[i]);
        }
        Writer writer = new Writer(path);
        writer.writeLines(saveSet);
    }

    public void save(String path) throws IOException {
        save(new File(path));
    }

    /**
     * This will remove hashMap file
     *
     * @param path path to file
     */
    public boolean delete(String path) {
        File toDelete = new File(path);
        return toDelete.delete();
    }

    /**
     * Sets distributor (char which divides node name from data).
     * See save method.
     *
     * @param newDistributor = char which divides node from data (for example in "node:data", ":" is distributor)
     * @return true if change was successful, false otherwise
     */
    public boolean setDistributor(String newDistributor) {//sets char which divides node from data (for example "node:data")
        if (!" ".equals(newDistributor)) {
            distributor = newDistributor;
            return true;
        } else return false;
    }

    public String getOrElse(String key, String alternative) {
        if (containsKey(key)) {
            return get(key);
        } else {
            return alternative;
        }
    }

    public int getOrElse(String key, int alternative) {
        if (containsKey(key)) {
            String data = get(key);
            try {
                return Integer.parseInt(data);
            } catch (Exception ex) {
                return alternative;
            }
        } else {
            return alternative;
        }
    }

    public float getOrElse(String key, float alternative) {
        if (containsKey(key)) {
            String data = get(key);
            try {
                return Float.parseFloat(data);
            } catch (Exception ex) {
                return alternative;
            }
        } else {
            return alternative;
        }
    }

    public boolean getOrElse(String key, boolean alternative) {
        if (containsKey(key)) {
            String data = get(key).toLowerCase().trim();
            return data.equals("true") || data.equals("yes");
        } else {
            return alternative;
        }
    }
}
