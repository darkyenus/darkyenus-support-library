package darkyenus.utils.collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class is implemented as two hash maps with flipped key and value.
 * That means that this has put and remove operations of (2), but get operations (1) (ALL against original HashMap speeds).
 * Since there is getSecondary method, which accepts Value type as key and returns Key, this might be interesting in some cases.
 * <p/>
 * Private property.
 * User: Darkyen
 * Date: 7/26/13
 * Time: 9:50 AM
 */
@SuppressWarnings({"SuspiciousMethodCalls", "UnusedDeclaration", "NullableProblems"})
public class TwoWayHashMap<K, V> implements Map<K, V> {

    private HashMap<K, V> primaryHashMap;
    private HashMap<V, K> secondaryHashMap;

    public TwoWayHashMap() {
        this(16);
    }

    public TwoWayHashMap(int size) {
        primaryHashMap = new HashMap<K, V>(size);
        secondaryHashMap = new HashMap<V, K>(size);
    }

    @Override
    public int size() {
        return primaryHashMap.size();
    }

    @Override
    public boolean isEmpty() {
        return primaryHashMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object o) {
        return primaryHashMap.containsKey(o);
    }

    @Override
    public boolean containsValue(Object o) {
        return secondaryHashMap.containsKey(o);
    }

    public boolean containsSecondaryKey(Object o) {
        return secondaryHashMap.containsKey(o);
    }

    public boolean containsSecondaryValue(Object o) {
        return primaryHashMap.containsKey(o);
    }

    @Override
    public V get(Object o) {
        return primaryHashMap.get(o);
    }

    public K getSecondary(Object o) {
        return secondaryHashMap.get(o);
    }

    @Override
    public V put(K k, V v) {
        V previousValue = primaryHashMap.put(k, v);
        secondaryHashMap.put(v, k);
        return previousValue;
    }

    public K putSecondary(V v, K k) {
        K previousValue = secondaryHashMap.put(v, k);
        primaryHashMap.put(k, v);
        return previousValue;
    }

    @Override
    public V remove(Object o) {
        V secondaryKey = primaryHashMap.remove(o);
        secondaryHashMap.remove(secondaryKey);
        return secondaryKey;
    }

    public K removeSecondary(Object o) {
        K primaryKey = secondaryHashMap.remove(o);
        primaryHashMap.remove(primaryKey);
        return primaryKey;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> map) {
        for (Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    public void putAllSecondary(Map<? extends V, ? extends K> map) {
        for (Entry<? extends V, ? extends K> entry : map.entrySet()) {
            putSecondary(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        primaryHashMap.clear();
        secondaryHashMap.clear();
    }

    @Override
    public Set<K> keySet() {
        return primaryHashMap.keySet();
    }

    public Set<V> keySetSecondary() {
        return secondaryHashMap.keySet();
    }

    /**
     * @return values of primary hash map
     * @deprecated use keySetSecondary instead
     */
    @Override
    @Deprecated
    public Collection<V> values() {
        return primaryHashMap.values();
    }

    /**
     * @return values of secondary hash map
     * @deprecated Use keySet() instead.
     */
    @Deprecated
    public Collection<K> valuesSecondary() {
        return secondaryHashMap.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return primaryHashMap.entrySet();
    }

    public Set<Entry<V, K>> entrySetSecondary() {
        return secondaryHashMap.entrySet();
    }
}
