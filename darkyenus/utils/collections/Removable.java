package darkyenus.utils.collections;

/**
 * Private property.
 * User: Darkyen
 * Date: 6/8/13
 * Time: 3:43 PM
 */
public interface Removable<M> extends Container<M> {
    public M remove();
}
