/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkyenus.utils.collections;

import darkyenus.utils.files.Reader;
import darkyenus.utils.files.Writer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Savable ArrayList of Strings.
 *
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class StringList extends ArrayList<String> {

    /**
     * Loads previously saved StringList, if file does not exists, is created
     *
     * @param path path to saved StringList or new StringList file
     * @throws IOException
     */
    public void load(String path) throws IOException {
        load(new File(path));
    }

    /**
     * Loads previously saved StringList, if file does not exists, is created
     *
     * @param file path to saved StringList or new StringList file
     * @throws IOException
     */
    public void load(File file) throws IOException {
        if (!file.isFile()) {
            new Writer(file).writeLines(new String[0]);
        }
        this.addAll(Arrays.asList(new Reader(file).readRest()));
    }

    /**
     * Clears StringList and loads from new location.
     *
     * @param path path to saved StringList or new StringList file
     * @throws IOException
     */
    public void reload(String path) throws IOException {
        reload(new File(path));
    }

    /**
     * Clears StringList and loads from new location.
     *
     * @param path path to saved StringList or new StringList file
     * @throws IOException
     */
    public void reload(File path) throws IOException {
        clear();
        load(path);
    }

    /**
     * Saves StringList into file, if file doesn't exist, is created
     *
     * @param path path to save StringList
     * @throws IOException
     */
    public void save(String path) throws IOException {
        save(new File(path));
    }

    /**
     * Saves StringList into file, if file doesn't exist, is created
     *
     * @param path path to save StringList
     * @throws IOException
     */
    public void save(File path) throws IOException {
        new Writer(path).writeLines(toArray(new String[size()]));
    }
}
