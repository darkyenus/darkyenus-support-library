/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkyenus.utils;

import java.awt.geom.Point2D;
import java.util.Random;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class MathUtil {

    public static final float TWO_PI = (float) (Math.PI * 2.);
    public static final Random RANDOM = new Random();

    /**
     * Get angle between 12 o'clock, first point and second point
     *
     * @param first  first point (center)
     * @param second second point
     * @return angle between 12 o'clock first point and second point in radians
     */
    public static double getAngleBetween(Point2D first, Point2D second) {
        return Math.atan2(second.getX() - first.getX(), -second.getY() + first.getY());
    }

    /**
     * Get angle between 12 o'clock, first point and second point
     *
     * @param x1 First point's x
     * @param y1 First point's y
     * @param x2 Second point's x
     * @param y2 Second point's y
     * @return angle between 12 o'clock first point and second point in radians
     */
    public static double getAngleBetween(double x1, double y1, double x2, double y2) {
        return Math.atan2(x2 - x1, -y2 + y1);
    }

    /**
     * Finds if 'first' equals to 'second' with given tolerance.<br>
     * Example:<br> equals(5,6,1) = true because 5 + 1 >= 6 equals(5,7,1) =
     * false because 5 + 1 < 7 <br> Calling with negative tolerance will always
     * return false.
     *
     * @param first     first number to compare
     * @param second    second number to compare
     * @param tolerance tolerance between two numbers
     * @return true if (first-tolerance<=second && first+tolerance>=second)
     */
    public static boolean equals(int first, int second, int tolerance) {
        return first - tolerance <= second && first + tolerance >= second;
    }

    /**
     * @see #equals(int, int, int)
     */
    public static boolean equals(float first, float second, float tolerance) {
        return first - tolerance <= second && first + tolerance >= second;
    }

    /**
     * Makes angle value from 0 to 359
     *
     * @param angle in degrees
     * @return nice looking value of angle (same if it is already nice)
     */
    public static double standardizeAngle(double angle) {
        double result = angle;
        while (result < 0) {
            result += 360;
        }
        while (result >= 360) {
            result -= 360;
        }
        return result;
    }

    /**
     * Makes angle value from 0 to 359
     *
     * @param angle in degrees
     * @return nice looking value of angle (same if it is already nice)
     */
    public static int standardizeAngle(int angle) {
        int result = angle;
        while (result < 0) {
            result += 360;
        }
        while (result >= 360) {
            result -= 360;
        }
        return result;
    }

    /**
     * Direction aware radian clipper.
     *
     * @param radian to clip to bounds
     * @return clipped input
     */
    public static float standardizeDirectionalRadian(float radian) {
        return radian % TWO_PI;
    }

    public static float standardizeRadian(float radian) {
        if (radian > 0) {
            return radian % TWO_PI;
        } else {
            return TWO_PI - (-radian % TWO_PI);
        }
    }

    /**
     * Get size of line which leads from center of rectangle to its intersection
     * with rectangle's side
     *
     * @param rectangleSize size of rectangle's sides
     * @param angle         of line (relatively to the 12 o'clock of rectangle,
     *                      clockwise) in degrees
     * @return size of line
     */
    public static double getLineInRectangleLengthByAngle(double rectangleSize, double angle) {
        double fixedAngle = angle;
        while (fixedAngle < 0) {
            fixedAngle += 360;
        }
        return (rectangleSize / 2) / Math.cos(Math.toRadians(Math.abs(fixedAngle % 90 - 45) - 45));
    }

    public static float getShortestRadianDifference(float from, float to) {
        float clock = standardizeRadian(to - from);
        float counterClock = standardizeRadian(TWO_PI + from - to);
        if (clock <= counterClock) {
            return clock;
        } else {
            return -counterClock;
        }
    }
}
