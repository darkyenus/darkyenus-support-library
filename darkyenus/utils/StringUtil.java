/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkyenus.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class StringUtil {

    public static int countOccurrences(String haystack, char needle) {
        int count = 0;
        for (int i = 0; i < haystack.length(); i++) {
            if (haystack.charAt(i) == needle) {
                count++;
            }
        }
        return count;
    }

    public static int findXthChar(String haystack, char needle, int xth) {
        int foundsofar = 0;
        if (xth < 1 || xth > haystack.length()) {
            return -1;
        }
        for (int index = 0; index < haystack.length(); index++) {
            if (haystack.charAt(index) == needle) {
                foundsofar++;
            }
            if (foundsofar == xth) {
                return index;
            }
        }
        return -1;
    }

    public static String getFileNameFromPath(String from) {
        int start = 0;
        int end = from.length();
        if (from.contains(File.separator)) {
            start = from.lastIndexOf(File.separator);
        }
        if (from.contains(".")) {
            end = from.lastIndexOf('.');
        }
        if (start > end) {
            return from;
        }
        return from.substring(start, end);
    }

    /**
     * Joins strings in string1|splitter|string2|splitter|string3... fashion.
     *
     * @param strings  strings to join
     * @param splitter string to put between strings
     * @return joined string
     */
    public static String join(Object[] strings, String splitter) {
        return join(strings, splitter, 0);
    }

    /**
     * @param strings  strings to join
     * @param splitter string to put between strings
     * @return joined string
     * @see StringUtil#join(Object[], String)
     *      Provided for compatibility.
     */
    public static String join(String[] strings, String splitter) {
        return join(strings, splitter, 0);
    }

    /**
     * Joins strings in string1|splitter|string2|splitter|string3... fashion.
     *
     * @param strings    strings to join
     * @param splitter   string to put between strings
     * @param startIndex from which index use strings (0 = whole array)
     * @return joined string
     */
    public static String join(Object[] strings, String splitter, int startIndex) {
        return join(strings, splitter, startIndex, strings.length);
    }

    /**
     * Joins strings in string1|splitter|string2|splitter|string3... fashion.
     *
     * @param strings    strings to join
     * @param splitter   string to put between strings
     * @param startIndex from which index use strings (0 = whole array)
     * @param endIndex   to which index (excluding) use strings (strings.length =
     *                   till end)
     * @return joined string
     */
    public static String join(Object[] strings, String splitter, int startIndex, int endIndex) {
        StringBuilder result = new StringBuilder();
        String actualSplitter = "";
        for (int i = startIndex; i < endIndex; i++) {
            result.append(actualSplitter).append(strings[i]);
            actualSplitter = splitter;
        }
        return result.toString();
    }

    public static String[] toStringArray(Object[] array) {
        String[] result = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i].toString();
        }
        return result;
    }

    public static String[] toStringArray(int[] array) {
        String[] result = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = Integer.toString(array[i]);
        }
        return result;
    }

    public static String[] toStringArray(byte[] array) {
        String[] result = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = Byte.toString(array[i]);
        }
        return result;
    }

    public static String[] toStringArray(long[] array) {
        String[] result = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = Long.toString(array[i]);
        }
        return result;
    }

    public static String[] toStringArray(float[] array) {
        String[] result = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = Float.toString(array[i]);
        }
        return result;
    }

    public static String[] toStringArray(double[] array) {
        String[] result = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = Double.toString(array[i]);
        }
        return result;
    }

    /**
     * This method will split text as String.split(space) would do, but it will group capsulated parts.
     * For example:
     * "Lazy (brown (fox)) jumped (over) (dog)."
     * Will split to:
     * "Lazy" "brown (fox)" "jumped" "over" "dog)."
     * Parameters in this example were " " for space, '(' for open and ')' for closed.
     * If text ends with unclosed capsule, it assumes that it is closed properly. (See example)
     *
     * @param text                  to split
     * @param space                 what you want to use as space character
     * @param open                  what you want to use as first capsulation character
     * @param close                 what you want to use as last capsulation character
     * @param omitDuplicateCapsules set this to true, if you want to parse "((something))" as "something" instead of default "(something)" (default false)
     * @return splitted string
     */
    public static String[] splitContextAware(String[] text, String space, char open, char close, boolean omitDuplicateCapsules) {
        ArrayList<String> result = new ArrayList<String>();
        StringBuilder capsule = new StringBuilder();
        int inCapsule = 0;
        int minLayer = Integer.MAX_VALUE;

        for (String part : text) {
            int howManyTimesOpen = 0;
            int howManyTimesClosed = 0;
            for (int i = 0; i < part.length(); i++) {
                if (part.charAt(i) == open) {
                    howManyTimesOpen++;
                } else {
                    break;
                }
            }
            for (int i = part.length() - 1; i >= 0; i--) {
                if (part.charAt(i) == close) {
                    howManyTimesClosed++;
                } else {
                    break;
                }
            }
/*
            int shared = Math.min(howManyTimesOpen, howManyTimesClosed);
            howManyTimesOpen -= shared;
            howManyTimesClosed -= shared;
            //At this point, one of lastly mentioned variables is 0*///Useless, it gets added together anyways

            //howManyTimesClosed = Math.min(inCapsule + howManyTimesOpen, howManyTimesClosed);//Prevent overclosing

            if (inCapsule != 0) {
                //IS in capsule
                inCapsule += howManyTimesOpen;
                inCapsule -= howManyTimesClosed;

                assert inCapsule >= 0 : "Overclosed!";

                if (inCapsule == 0) {
                    //Capsule stopped
                    capsule.append(space);
                    capsule.append(part);

                    result.add(decapsulate(capsule.toString(), open, close, minLayer, omitDuplicateCapsules));
                    capsule = new StringBuilder();
                } else {
                    //Capsule continues
                    capsule.append(space).append(part);
                    minLayer = Math.min(inCapsule, minLayer);
                }
            } else {
                inCapsule += howManyTimesOpen;
                inCapsule -= howManyTimesClosed;

                assert inCapsule >= 0 : "Overclosed!";

                if (inCapsule != 0) {
                    //Capsule started
                    capsule.append(part);
                } else {
                    //Capsule ended right where it started
                    result.add(decapsulate(part, open, close, minLayer, omitDuplicateCapsules));
                }
            }
        }

        assert inCapsule == 0 : "Text not properly closed, message overflow: " + capsule.toString();

        return result.toArray(new String[result.size()]);
    }

    private static String decapsulate(String capsule, char open, char close, int minimalLayer, boolean omitDupes) {
        int howManyTimesOpen = 0;
        int howManyTimesClosed = 0;
        for (int y = 0; y < capsule.length(); y++) {
            if (capsule.charAt(y) == open) {
                howManyTimesOpen++;
            } else {
                break;
            }
        }
        for (int y = capsule.length() - 1; y >= 0; y--) {
            if (capsule.charAt(y) == close) {
                howManyTimesClosed++;
            } else {
                break;
            }
        }

        int howManyTimesCapsulated = Math.min(minimalLayer, Math.min(howManyTimesOpen, howManyTimesClosed));

        if (!omitDupes) {
            howManyTimesCapsulated = Math.min(1, howManyTimesCapsulated);
        }

        int startOffset = howManyTimesCapsulated;
        int endOffset = capsule.length() - howManyTimesCapsulated;
        return capsule.substring(startOffset, endOffset);
    }

    public static String[] splitContextAware(String[] text, int offset, String space, char open, char close, boolean omitDuplicateCapsules) {
        String[] basicSplit = Arrays.copyOfRange(text, offset, text.length);
        return splitContextAware(basicSplit, space, open, close, omitDuplicateCapsules);
    }

    public static String[] splitContextAware(String[] text, int offset, int length, String space, char open, char close, boolean omitDuplicateCapsules) {
        String[] basicSplit = Arrays.copyOfRange(text, offset, Math.min(offset + length, text.length));
        return splitContextAware(basicSplit, space, open, close, omitDuplicateCapsules);
    }

    public static String[] splitContextAware(String text, String space, char open, char close, boolean omitDuplicateCapsules) {
        String[] basicSplit = text.split(space);
        return splitContextAware(basicSplit, space, open, close, omitDuplicateCapsules);
    }

    public static String[] processStringArray(String[] array, StringProcessor processor) {
        String[] result = new String[array.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = processor.processLine(array[i]);
        }
        return result;
    }

    /**
     * Returns how many chars are matching on position. Case insensitive.
     * For example:
     * aaa
     * Aab
     * Would return 2.
     *
     * @param first  string
     * @param second string
     * @return how many characters are same
     */
    public static int getMatchingChars(String first, String second) {
        int result = 0;
        for (int i = 0; i < Math.min(first.length(), second.length()); i++) {
            if (first.toLowerCase().charAt(i) == second.toLowerCase().charAt(i)) {
                result++;
            } else {
                break;
            }
        }
        return result;
    }

    private static int minimum(int a, int b, int c) {
        if (a <= b && a <= c)
            return a;
        if (b <= a && b <= c)
            return b;
        return c;
    }

    /**
     * Computes Levenshtein distance of two strings.
     * (Minimum number of single character modifications needed to get second string from first)
     * Author: Meta @ vidasConcurrentes
     *
     * @param str1       first string
     * @param str2       second string
     * @param add        weight of add operation (1 is default, higher it is, more "expensive" this operation is)
     * @param delete     weight of delete operation
     * @param substitute weight of substitute operation
     * @return Levenshtein distance of two strings using given weights
     * @see darkyenus.utils.StringUtil#computeLevenshteinDistance(char[], char[], int, int, int)
     */
    public static int computeLevenshteinDistance(String str1, String str2, int add, int delete, int substitute) {
        return computeLevenshteinDistance(str1.toCharArray(), str2.toCharArray(), add, delete, substitute);
    }

    /*
     * Author: Meta @ vidasConcurrentes
     * Related to: http://vidasconcurrentes.blogspot.com/2011/06/distancia-de-levenshtein-distancia-de.html
     *
     * This is the class which implements the Weighted Levenshtein Distance
     * To do so, we take the base algorithm and make some modifications, as follows:
     *              ·       multiply first column for the Delete weight
     *              ·       multiply first row for the Insert weight
     *              ·       add the Delete weight when checking for the [i-1][j] value
     *              ·       add the Insert weight when checking for the [i][j-1] value
     *              ·       make the substitution cost the Substitution weight
     */
    private static int computeLevenshteinDistance(char[] str1, char[] str2, int insert, int delete, int substitute) {
        int[][] distance = new int[str1.length + 1][str2.length + 1];

        for (int i = 0; i <= str1.length; i++)
            distance[i][0] = i * delete;    // non-weighted algorithm doesn't take Delete weight into account
        for (int j = 0; j <= str2.length; j++)
            distance[0][j] = j * insert;    // non-weighted algorithm doesn't take Insert weight into account
        for (int i = 1; i <= str1.length; i++) {
            for (int j = 1; j <= str2.length; j++) {
                distance[i][j] = minimum(distance[i - 1][j] + delete,      // would be +1 instead of +delete
                        distance[i][j - 1] + insert,                                      // would be +1 instead of +insert
                        distance[i - 1][j - 1] + ((str1[i - 1] == str2[j - 1]) ? 0 : substitute));      // would be 1 instead of substitute
            }
        }
        return distance[str1.length][str2.length];
    }

    public interface StringProcessor {
        public String processLine(String line);
    }

}
