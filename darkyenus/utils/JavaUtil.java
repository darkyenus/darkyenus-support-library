/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkyenus.utils;

/**
 * @author Darkyen
 */
public class JavaUtil {

    public static <R extends Enum<R>> R matchEnumMember(Class<R> enumToSearch, String matchString) {
        R found = null;
        int bestMatch = 0;
        boolean sure = false;

        for (R toTry : enumToSearch.getEnumConstants()) {
            if (toTry.toString().equalsIgnoreCase(matchString)) {
                return toTry;
            } else {
                int match = getMatchingChars(matchString, toTry.toString());
                if (match == bestMatch) {
                    sure = false;
                } else if (match > bestMatch) {
                    bestMatch = match;
                    found = toTry;
                    sure = true;
                }
            }
        }

        if (sure) {
            return found;
        } else {
            return null;
        }
    }

    private static int getMatchingChars(String one, String two) {
        int result = 0;
        for (int i = 0; i < one.length(); i++) {
            try {
                if (one.toLowerCase().charAt(i) == two.toLowerCase().charAt(i)) {
                    result++;
                } else {
                    break;
                }
            } catch (StringIndexOutOfBoundsException e) {
                break;//Out of string, wont happen frequently. probably
            }
        }
        return result;
    }
}
