package darkyenus.utils;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/29/13
 * Time: 8:21 PM
 */
public class DebugUtil {
    public static String getMyCaller(int depth) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        StringBuilder result = null;
        for (int i = 3; i < Math.min(stackTraceElements.length, 3 + depth); i++) {
            if (result == null) {
                result = new StringBuilder();
            } else {
                result.append(" < ");
            }
            result.append(stackTraceElements[i].getMethodName()).append("[");
            String basicName = stackTraceElements[i].getFileName();
            if (basicName.toLowerCase().endsWith(".java")) {
                basicName = basicName.substring(0, basicName.length() - 5);
            }
            result.append(basicName);
            if (!stackTraceElements[i].isNativeMethod()) {
                result.append(":").append(stackTraceElements[i].getLineNumber());
            }
            result.append("]");
        }
        if (result == null) {
            return "";
        } else {
            return result.toString();
        }
    }
}
