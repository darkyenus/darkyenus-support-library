package darkyenus.utils;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/10/13
 * Time: 12:17 PM
 */
public class InterpolationUtils {

    public static float getInterpolationRatio(long start, long stop, long now) {
        long delta = stop - start;
        long offset = now - start;
        return (float) (((double) offset) / ((double) delta));
    }

    public static float interpolateLinear(float first, float second, float ratio) {
        float delta = second - first;
        return first + (delta * ratio);
    }

    public static float interpolateRadian(float first, float second, float interpolateTo) {
        if (MathUtil.equals(first, second, 0.0001f)) {
            return second;//This piece of code is essential, since following code breaks when working with too small numbers in some cases
        }
        float deltaToNewRot = MathUtil.getShortestRadianDifference(first, second);
        return first + (deltaToNewRot * interpolateTo);
    }
}
