/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkyenus.utils;

import java.awt.*;

/**
 * @author Darkyen
 */
public class CollisionUtil {

    /**
     * Utility method to determine whether circle is contained by or intersects rectangle.
     *
     * @param rect
     * @param circle
     * @param circleRadius
     * @return true if two shapes collide
     */
    public static boolean intersectsRectangleCircle(Rectangle rect, Point circle, int circleRadius) {
        double circleDistancex = Math.abs(circle.x - rect.x - rect.width / 2);
        double circleDistancey = Math.abs(circle.y - rect.y - rect.height / 2);

        if (circleDistancex > (rect.width / 2 + circleRadius)) {
            return false;
        }
        if (circleDistancey > (rect.height / 2 + circleRadius)) {
            return false;
        }

        if (circleDistancex <= (rect.width / 2)) {
            return true;
        }
        if (circleDistancey <= (rect.height / 2)) {
            return true;
        }

        double cornerDistance_sq = Math.pow(circleDistancex - rect.width / 2, 2)
                + Math.pow(circleDistancey - rect.height / 2, 2);

        return cornerDistance_sq <= (circleRadius ^ 2);
    }
}
