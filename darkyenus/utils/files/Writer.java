/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkyenus.utils.files;

import java.io.*;

/**
 * @author Darkyenus
 */
public class Writer {

    File file;

    public Writer(File file) throws IOException {
        this.file = file;
        if (this.file.getParentFile() != null) {
            this.file.getParentFile().mkdirs();
        }
        this.file.createNewFile();
    }

    public Writer(String path) throws IOException {
        this(new File(path));
    }

    /**
     * Writes lines into path specified in constructor. If file exists, will be
     * replaced, otherwise it will be created.
     *
     * @param lines
     * @throws IOException
     */
    public void writeLines(String[] lines) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
        for (int i = 0; i < lines.length; i++) {
            bw.write(lines[i]);
            bw.newLine();
        }
        bw.flush();
        bw.close();
    }

    public void write(String string) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
        bw.write(string);
        bw.flush();
        bw.close();
    }
}
