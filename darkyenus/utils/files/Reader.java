/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkyenus.utils.files;

import java.io.*;
import java.util.ArrayList;

/**
 * @author Darkyenus
 */
public class Reader {

    private BufferedReader br;
    private boolean closed = false;

    /**
     * Construct new Reader with path to file
     *
     * @param path to file
     * @throws FileNotFoundException when file doesn't exist
     */
    public Reader(String path) throws FileNotFoundException {
        try {
            br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(path)), "UTF-8"));
        } catch (UnsupportedEncodingException ignored) {
        }
    }

    /**
     * Construct new Reader which will read from file
     *
     * @param file to read from
     * @throws FileNotFoundException when file doesn't exist
     */
    public Reader(File file) throws FileNotFoundException {
        try {
            br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(file)), "UTF-8"));
        } catch (UnsupportedEncodingException ignored) {
        }
    }

    /**
     * Read next line of document. If is on last line, auto closes itself and
     * returns null.
     *
     * @throws IOException
     */
    public String readNext() throws IOException {
        String line = br.readLine();
        if (line == null) {
            br.close();
            closed = true;
        }
        return line;
    }

    /**
     * Call to manually close file.
     *
     * @throws IOException
     */
    public void close() throws IOException {
        if (!closed) {
            br.close();
        }
    }

    /**
     * Loads rest of file into array by calling readNext()
     *
     * @return rest of file
     */
    public String[] readRest() throws IOException {
        ArrayList<String> rest = new ArrayList<String>();
        String buffer = readNext();
        while (buffer != null) {
            rest.add(buffer);
            buffer = readNext();
        }
        return rest.toArray(new String[rest.size()]);
    }
}
