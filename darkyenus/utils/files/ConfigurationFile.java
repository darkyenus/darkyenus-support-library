/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkyenus.utils.files;

import darkyenus.utils.collections.StringMap;

import java.io.File;
import java.util.logging.Logger;

/**
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class ConfigurationFile extends StringMap {
    @SuppressWarnings("NonConstantLogger")
    private final Logger logger;
    private String[] defaultConfiguration;
    private File path;

    public ConfigurationFile(File path, Logger logger, String... defaultConfiguration) {
        this.path = path;
        this.logger = logger;
        this.defaultConfiguration = defaultConfiguration;
    }

    public ConfigurationFile(String path, Logger logger, String... defaultConfiguration) {
        this(new File(path), logger, defaultConfiguration);
    }

    public ConfigurationFile(String path, Logger logger) {
        this(path, logger, new String[0]);
    }

    public ConfigurationFile(File path, String... defaultConfiguration) {
        this(path, null, defaultConfiguration);
    }

    public ConfigurationFile(String path, String... defaultConfiguration) {
        this(path, null, defaultConfiguration);
    }

    public ConfigurationFile(String path) {
        this(path, null, new String[0]);
    }

    public void load() {
        try {
            loadOrCreate(path, defaultConfiguration);
        } catch (Exception iOException) {
            if (logger != null) {
                logger.severe("Could not load ConfigurationFile: " + iOException);
            } else {
                System.out.println("Could not load ConfigurationFile: " + iOException);
            }
        }
    }

    public void save() {
        try {
            save(path);
        } catch (Exception iOException) {
            if (logger != null) {
                logger.severe("Could not save ConfigurationFile: " + iOException);
            } else {
                System.out.println("Could not save ConfigurationFile: " + iOException);
            }
        }
    }
}