/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkyenus.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author Darkyen
 */
public class ImageUtil {

    public static BufferedImage loadImage(String path) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsConfiguration gc = ge.getDefaultScreenDevice().getDefaultConfiguration();
        try {
            File file = new File(path);

            if (!file.exists() || !file.canRead()) {
                System.out.println("File not found : " + path);
                return null;
            }

            BufferedImage im = ImageIO.read(file);

            int transparency = im.getColorModel().getTransparency();
            BufferedImage copy = gc.createCompatibleImage(im.getWidth(), im.getHeight(), transparency);
            Graphics2D g2d = copy.createGraphics();
            g2d.drawImage(im, 0, 0, null);
            g2d.dispose();
            return copy;
        } catch (IOException e) {
            System.out.println("Image loading error : " + path + ":\n" + e + ":" + e.getStackTrace());
            return null;
        }
    }
}
