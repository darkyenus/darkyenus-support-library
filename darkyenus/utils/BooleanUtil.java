package darkyenus.utils;

/**
 * Private property.
 * User: Darkyen
 * Date: 7/31/13
 * Time: 5:41 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class BooleanUtil {
    public static int countTrue(boolean... booleans) {
        int count = 0;
        for (boolean bool : booleans) {
            if (bool) count++;
        }
        return count;
    }
}
