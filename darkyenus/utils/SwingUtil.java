/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package darkyenus.utils;

import javax.swing.*;
import java.awt.*;

/**
 * @author Darkyen
 */
public class SwingUtil {
    public static void getMeToCenter(JFrame window) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int w = window.getSize().width;
        int h = window.getSize().height;
        int x = (dim.width - w) / 2;
        int y = (dim.height - h) / 2;
        window.setLocation(x, y);
    }

    public static Dimension getMaxWindowDimension(){
        GraphicsConfiguration config = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        int left = Toolkit.getDefaultToolkit().getScreenInsets(config).left;
        int right = Toolkit.getDefaultToolkit().getScreenInsets(config).right;
        int top = Toolkit.getDefaultToolkit().getScreenInsets(config).top;
        int bottom = Toolkit.getDefaultToolkit().getScreenInsets(config).bottom;

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        screenSize.width = screenSize.width - left - right;
        screenSize.height = screenSize.height - top - bottom;
        return screenSize;
    }
}
